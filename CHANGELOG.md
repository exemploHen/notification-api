
## 1.0.0 - 21/08/2018
- Rest de envio simples de email
- Documentação swagger
- Implementação de fila
- Integração com o Postgres
- Integração do campo JSON do banco
- Rest de pesquisa dinâmica de json
- Arquivo de configuração fora da aplicação
- Fila de envio normal delimitada com horários
- README.md
