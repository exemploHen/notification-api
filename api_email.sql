-- Schema creation
CREATE SCHEMA apicommunication;
ALTER SCHEMA apicommunication OWNER TO postgres;

-- Table creation
create table apicommunication.batch_notification (
  id_batch bigint not null,
  date_created timestamp not null default current_timestamp,
  status int,
  CONSTRAINT pk_batch_notification PRIMARY KEY (id_batch)
)
WITH (
  OIDS = FALSE
);
create sequence apicommunication.seq_id_batch_notification;

create table apicommunication.EMAIL_NOTIFICATION (
  id_email_notification bigint not null,
  external_id jsonb,
  date_created timestamp not null default current_timestamp,
  date_processed timestamp,
  status int,
  prioritary int default 0,
  request_origin varchar(100),
  recipient varchar(100),
  subject varchar(100),
  id_mailgun varchar(200),
  mailgun_status varchar(200),
  id_batch bigint,
  CONSTRAINT pk_email_notification PRIMARY KEY (id_email_notification),
  CONSTRAINT fk_email_batch_notification FOREIGN KEY (id_batch) REFERENCES apicommunication.batch_notification(id_batch) ON UPDATE NO ACTION ON DELETE NO ACTION
) WITH (
  OIDS = FALSE
);
create sequence apicommunication.seq_id_email_notification;

create table apicommunication.user
(
  id_user      bigint       not null,
  name         varchar(100) not null,
  email        varchar(100) not null,
  password     varchar(500) not null,
  status       int          not null,
  api_key      varchar(500),
  date_created timestamp    not null default current_timestamp,
  constraint pk_user primary key (id_user)
) WITH (
    OIDS = FALSE
  );
create sequence apicommunication.seq_id_user;



