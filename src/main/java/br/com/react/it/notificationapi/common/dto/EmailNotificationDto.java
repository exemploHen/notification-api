package br.com.react.it.notificationapi.common.dto;

import br.com.react.it.notificationapi.common.model.EmailNotificationEntity;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.time.LocalDateTime;

public class EmailNotificationDto implements Serializable {

  private static final long serialVersionUID = 5978630435300245731L;

  public EmailNotificationDto(EmailNotificationEntity entity) {
    this.id = entity.getId();
    this.externalId = entity.getExternalId();
    this.dateCreated = entity.getDateCreated();
    this.dateProcessed = entity.getDateProcessed();
    this.status = entity.getStatus();
    this.requestOrigin = entity.getRequestOrigin();
    this.recipient = entity.getRecipient();
    this.subject = entity.getSubject();
    this.idBatch = entity.getIdBatch();
    this.idMailgun = entity.getIdMailgun();
    this.mailgunStatus = entity.getMailgunStatus();
  }

  private Long id;

  @JsonProperty(value = "external_id")
  private Object externalId;

  private LocalDateTime dateCreated;

  private LocalDateTime dateProcessed;

  private Integer status;

  private String requestOrigin;

  private String recipient;

  private String subject;

  private Long idBatch;

  private String idMailgun;

  private String mailgunStatus;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Object getExternalId() {
    return externalId;
  }

  public void setExternalId(Object externalId) {
    this.externalId = externalId;
  }

  public LocalDateTime getDateCreated() {
    return dateCreated;
  }

  public void setDateCreated(LocalDateTime dateCreated) {
    this.dateCreated = dateCreated;
  }

  public LocalDateTime getDateProcessed() {
    return dateProcessed;
  }

  public void setDateProcessed(LocalDateTime dateProcessed) {
    this.dateProcessed = dateProcessed;
  }

  public Integer getStatus() {
    return status;
  }

  public void setStatus(Integer status) {
    this.status = status;
  }

  public String getRequestOrigin() {
    return requestOrigin;
  }

  public void setRequestOrigin(String requestOrigin) {
    this.requestOrigin = requestOrigin;
  }

  public String getRecipient() {
    return recipient;
  }

  public void setRecipient(String recipient) {
    this.recipient = recipient;
  }

  public String getSubject() {
    return subject;
  }

  public void setSubject(String subject) {
    this.subject = subject;
  }

  public Long getIdBatch() {
    return idBatch;
  }

  public void setIdBatch(Long idBatch) {
    this.idBatch = idBatch;
  }

  public String getIdMailgun() {
    return idMailgun;
  }

  public void setIdMailgun(String idMailgun) {
    this.idMailgun = idMailgun;
  }

  public String getMailgunStatus() {
    return mailgunStatus;
  }

  public void setMailgunStatus(String mailgunStatus) {
    this.mailgunStatus = mailgunStatus;
  }

  @Override
  public String toString() {
    return "EmailNotificationDto{" +
        "id=" + id +
        ", externalId=" + externalId +
        ", dateCreated=" + dateCreated +
        ", dateProcessed=" + dateProcessed +
        ", status=" + status +
        ", requestOrigin='" + requestOrigin + '\'' +
        ", recipient='" + recipient + '\'' +
        ", subject='" + subject + '\'' +
        ", idBatch=" + idBatch +
        ", idMailgun='" + idMailgun + '\'' +
        ", mailgunStatus='" + mailgunStatus + '\'' +
        '}';
  }
}
