package br.com.react.it.notificationapi.sender.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

public class AttachmentRequestDto implements Serializable {
  private static final long serialVersionUID = 8920527593029404580L;

  @ApiModelProperty(name = "Nome do anexo")
  @JsonProperty(value = "attachment_name")
  private String attachmentName;

  @ApiModelProperty(name = "anexo em Base64")
  private String attachment;

  public String getAttachmentName() {
    return attachmentName;
  }

  public void setAttachmentName(String attachmentName) {
    this.attachmentName = attachmentName;
  }

  public String getAttachment() {
    return attachment;
  }

  public void setAttachment(String attachment) {
    this.attachment = attachment;
  }
}
