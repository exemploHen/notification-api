package br.com.react.it.notificationapi.sender.dto;

import java.io.Serializable;

public class CallbackMessageDetailsDto implements Serializable {
  private static final long serialVersionUID = -1544112291261655842L;

  private CallbackHeadersDto headers;

  public CallbackHeadersDto getHeaders() {
    return headers;
  }

  public void setHeaders(CallbackHeadersDto headers) {
    this.headers = headers;
  }
}
