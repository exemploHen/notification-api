package br.com.react.it.notificationapi.sender.configuration;

import br.com.react.it.notificationapi.sender.authentication.ApiAuthenticationProvider;
import br.com.react.it.notificationapi.sender.authentication.ApiKeyAuthenticationFilter;
import br.com.react.it.notificationapi.sender.authentication.ApiLoginAuthenticationFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@Configuration
@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

  private final ApiAuthenticationProvider authenticationProvider;

  @Autowired
  public SecurityConfiguration(ApiAuthenticationProvider authenticationProvider) {
    this.authenticationProvider = authenticationProvider;
  }

  @Override
  protected void configure(final AuthenticationManagerBuilder auth) {
    auth.authenticationProvider(authenticationProvider);
  }

  @Override
  protected void configure(HttpSecurity http) throws Exception {
    http
        .csrf().disable()
        .exceptionHandling()
        .and()
        .addFilterBefore(new ApiKeyAuthenticationFilter("/api/email/**", authenticationManager()), UsernamePasswordAuthenticationFilter.class)
        .addFilterBefore(new ApiLoginAuthenticationFilter("/api/user/login", authenticationManager()), UsernamePasswordAuthenticationFilter.class)
        .authorizeRequests()
        .antMatchers("/api/callback").permitAll()
        .antMatchers("/api/support/**").permitAll();
  }
}
