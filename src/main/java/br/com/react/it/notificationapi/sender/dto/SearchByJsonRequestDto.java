package br.com.react.it.notificationapi.sender.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

public class SearchByJsonRequestDto implements Serializable {
  private static final long serialVersionUID = 9104815798633132053L;

  @ApiModelProperty(name = "Id externo", required = true)
  @JsonProperty(value = "external_id")
  @NotNull
  private Object externalId;

  public Object getExternalId() {
    return externalId;
  }

  public void setExternalId(Object externalId) {
    this.externalId = externalId;
  }
}
