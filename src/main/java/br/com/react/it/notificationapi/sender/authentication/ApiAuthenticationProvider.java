package br.com.react.it.notificationapi.sender.authentication;

import br.com.react.it.notificationapi.common.model.UserEntity;
import br.com.react.it.notificationapi.common.service.UserRolesService;
import br.com.react.it.notificationapi.common.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.List;

@Component
public class ApiAuthenticationProvider implements AuthenticationProvider {

  private UserService userService;

  private UserRolesService userRolesService;

  @Autowired
  public ApiAuthenticationProvider(UserService userService, UserRolesService userRolesService) {
    this.userService = userService;
    this.userRolesService = userRolesService;
  }

  @Override
  public Authentication authenticate(Authentication authentication) throws AuthenticationException {
    String name = authentication.getName();
    String password = authentication.getCredentials().toString();

    ApiUserDetails user;

    if (StringUtils.isEmpty(password)) {
      if (name.contains("Bearer ")) {
        name = name.replace("Bearer ", "");
      }
      user = apiKeyAuthentication(name);
    } else {
      user = loginAuthentication(name, password);
    }

    return new UsernamePasswordAuthenticationToken(user, password, user.getAuthorities());
  }

  private ApiUserDetails loginAuthentication(String name, String password) {
    try {
      UserEntity userEntity = userService.retrieveUserAndCheckPassword(name, password);
      List<String> roles = retrieveRoles(userEntity.getIdUser());
      return new ApiUserDetails(userEntity, roles);
    } catch (Exception e) {
      throw new UsernameNotFoundException("Usuário e/ou senha não existem!!");
    }
  }

  private List<String> retrieveRoles(Long userEntity) {
    return userRolesService.retrieveRolesFromIdUser(userEntity);
  }

  private ApiUserDetails apiKeyAuthentication(String apiKey) {
    try {
      UserEntity userEntity = userService.searchUserByApiKey(apiKey);
      List<String> roles = retrieveRoles(userEntity.getIdUser());
      return new ApiUserDetails(userEntity, roles);
    } catch (Exception e) {
      throw new UsernameNotFoundException("Usuário não encontrado!!");
    }
  }

  @Override
  public boolean supports(Class<?> aClass) {
    return aClass.equals(UsernamePasswordAuthenticationToken.class);
  }
}
