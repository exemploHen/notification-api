package br.com.react.it.notificationapi.sender.dto;

import br.com.react.it.notificationapi.common.dto.EmailNotificationDto;

public class SearchOneResponseDto extends ResponseDto{
  private static final long serialVersionUID = -262307175067553148L;

  private EmailNotificationDto email;

  public EmailNotificationDto getEmail() {
    return email;
  }

  public void setEmail(EmailNotificationDto email) {
    this.email = email;
  }

  @Override
  public String toString() {
    return "ResponseDto{" +
        "message='" + getMessage() + '\'' +
        ", code=" + getCode() +
        ", emails=" + email +
        '}';
  }
}
