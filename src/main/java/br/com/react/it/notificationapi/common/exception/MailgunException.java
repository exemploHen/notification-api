package br.com.react.it.notificationapi.common.exception;

public class MailgunException extends RuntimeException {

  private static final long serialVersionUID = 1949289868703922140L;

  public MailgunException(String message) {
    super(message);
  }
}
