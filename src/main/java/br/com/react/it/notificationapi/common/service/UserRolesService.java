package br.com.react.it.notificationapi.common.service;

import br.com.react.it.notificationapi.common.model.UserRolesEntity;
import br.com.react.it.notificationapi.common.repository.UserRolesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserRolesService {

  private UserRolesRepository userRolesRepository;

  private static final String ADMIN_ROLE = "ADMIN";

  @Autowired
  public UserRolesService(UserRolesRepository userRolesRepository) {
    this.userRolesRepository = userRolesRepository;
  }

  public boolean isUserAdmin(Long idUser) {
    List<UserRolesEntity> collect = findUserRolesFromIdUser(idUser).stream()
        .filter(item -> ADMIN_ROLE.equals(item.getRole()))
        .collect(Collectors.toList());
    return !collect.isEmpty();
  }

  private List<UserRolesEntity> findUserRolesFromIdUser(Long idUser) {
    return userRolesRepository.findByIdUser(idUser);
  }

  public List<String> retrieveRolesFromIdUser(Long idUser) {
    return userRolesRepository
        .findByIdUser(idUser).stream()
        .map(UserRolesEntity::getRole)
        .collect(Collectors.toList());
  }
}
