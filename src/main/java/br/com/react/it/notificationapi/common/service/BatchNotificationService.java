package br.com.react.it.notificationapi.common.service;

import br.com.react.it.notificationapi.common.model.BatchNotificationEntity;
import br.com.react.it.notificationapi.common.repository.BatchNotificationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
public class BatchNotificationService {

  private BatchNotificationRepository repository;

  private static final Integer PROCESSING = 1;

  @Autowired
  public BatchNotificationService(BatchNotificationRepository repository) {
    this.repository = repository;
  }

  public BatchNotificationEntity createNewBatch() {
    final BatchNotificationEntity entity = new BatchNotificationEntity();
    entity.setDateCreated(LocalDateTime.now());
    entity.setStatus(PROCESSING);
    return repository.save(entity);
  }
}
