package br.com.react.it.notificationapi.common.repository.impl;

import br.com.react.it.notificationapi.common.model.EmailNotificationEntity;
import br.com.react.it.notificationapi.common.repository.CustomEmailNotificationRepository;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;
import java.util.Map;

@Repository
public class CustomEmailNotificationRepositoryImpl implements CustomEmailNotificationRepository {

  private static final String QUERY = "SELECT eme.* FROM APICOMMUNICATION.EMAIL_NOTIFICATION eme";

  @PersistenceContext
  EntityManager entityManager;

  @Override
  public List searchByJsonProperty(Long idUser, Map map) {
    String queryJson = QUERY;
    queryJson += " WHERE id_user = " + idUser;
    StringBuilder queryJsonBuilder = new StringBuilder(queryJson);

    map.forEach((key, value) -> {
      queryJsonBuilder.append(" AND ").append(" eme.external_id->>'").append(key).append("' = '").append(value);
    });
    queryJson = queryJsonBuilder.toString();
    return entityManager.createNativeQuery(queryJson, EmailNotificationEntity.class).getResultList();
  }
}
