package br.com.react.it.notificationapi.common.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.Arrays;

public class InlineQueueDto implements Serializable {
  private static final long serialVersionUID = -8281910261458649729L;

  private byte[] inline;

  @JsonProperty(value = "inline_name")
  private String inlineName;

  public byte[] getInline() {
    return inline;
  }

  public void setInline(byte[] inline) {
    this.inline = inline;
  }

  public String getInlineName() {
    return inlineName;
  }

  public void setInlineName(String inlineName) {
    this.inlineName = inlineName;
  }

  @Override
  public String toString() {
    return "InlineQueueDto{" +
        "inline=" + Arrays.toString(inline) +
        '}';
  }
}
