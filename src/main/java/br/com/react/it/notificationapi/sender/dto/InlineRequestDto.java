package br.com.react.it.notificationapi.sender.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class InlineRequestDto implements Serializable {

  private static final long serialVersionUID = 3916336121660118559L;

  private String inline;

  @JsonProperty(value = "inline_name")
  private String inlineName;

  public String getInline() {
    return inline;
  }

  public void setInline(String inline) {
    this.inline = inline;
  }

  public String getInlineName() {
    return inlineName;
  }

  public void setInlineName(String inlineName) {
    this.inlineName = inlineName;
  }

}
