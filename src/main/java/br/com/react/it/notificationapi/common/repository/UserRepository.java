package br.com.react.it.notificationapi.common.repository;

import br.com.react.it.notificationapi.common.model.UserEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends CrudRepository<UserEntity, Long> {

  UserEntity findByApiKeyAndStatus(String apiKey, Integer status);

  UserEntity findByEmail(String email);

  UserEntity findByApiKey(String apiKey);
}
