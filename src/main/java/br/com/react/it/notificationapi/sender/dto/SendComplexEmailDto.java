package br.com.react.it.notificationapi.sender.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

public class SendComplexEmailDto implements Serializable {
  private static final long serialVersionUID = 8763440130945849996L;

  @ApiModelProperty(name = "Mensagem", required = true)
  @NotNull
  private String message;

  @ApiModelProperty(name = "Email", required = true)
  @NotNull
  private String email;

  @ApiModelProperty(name = "Aplicação de origem", required = true)
  @NotNull
  private String origin;

  private String subject;

  @ApiModelProperty(name = "Lista de Anexos")
  private List<AttachmentRequestDto> attachments;

  private String html;

  @ApiModelProperty(name = "Id externo")
  @JsonProperty(value = "external_id")
  private Object externalId;

  @ApiModelProperty(name = "Imagens do corpo do email")
  private List<InlineRequestDto> inlines;

  private String from;

  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getOrigin() {
    return origin;
  }

  public void setOrigin(String origin) {
    this.origin = origin;
  }

  public String getSubject() {
    return subject;
  }

  public void setSubject(String subject) {
    this.subject = subject;
  }

  public List<AttachmentRequestDto> getAttachments() {
    return attachments;
  }

  public void setAttachments(List<AttachmentRequestDto> attachments) {
    this.attachments = attachments;
  }

  public String getHtml() {
    return html;
  }

  public void setHtml(String html) {
    this.html = html;
  }

  public Object getExternalId() {
    return externalId;
  }

  public void setExternalId(Object externalId) {
    this.externalId = externalId;
  }

  public List<InlineRequestDto> getInlines() {
    return inlines;
  }

  public void setInlines(List<InlineRequestDto> inlines) {
    this.inlines = inlines;
  }

  public String getFrom() {
    return from;
  }

  public void setFrom(String from) {
    this.from = from;
  }

  @Override
  public String toString() {
    return "SendComplexEmailDto{" +
        "message='" + message + '\'' +
        ", email='" + email + '\'' +
        ", origin='" + origin + '\'' +
        ", subject='" + subject + '\'' +
        ", html='" + html + '\'' +
        ", externalId=" + externalId +
        ", from='" + from + '\'' +
        '}';
  }
}
