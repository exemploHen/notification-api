package br.com.react.it.notificationapi.sender.authentication;

import br.com.react.it.notificationapi.sender.dto.LoginRequestDto;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collections;

public class ApiLoginAuthenticationFilter extends AbstractAuthenticationProcessingFilter {

  public ApiLoginAuthenticationFilter(String url, AuthenticationManager authManager) {
    super(new AntPathRequestMatcher(url));
    setAuthenticationManager(authManager);
  }

  @Override
  public Authentication attemptAuthentication(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws AuthenticationException, IOException, ServletException {
    LoginRequestDto loginRequestDto = new ObjectMapper().readValue(httpServletRequest.getInputStream(), LoginRequestDto.class);
    return getAuthenticationManager().authenticate(new UsernamePasswordAuthenticationToken(
        loginRequestDto.getLogin(), loginRequestDto.getPassword(), Collections.emptyList()));
  }

  @Override
  protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain, Authentication authResult) throws IOException, ServletException {
    String apiKey = ((ApiUserDetails) authResult.getDetails()).getKey();
    response.addHeader("Authorization", "Bearer " + apiKey);
    chain.doFilter(request, response);
  }
}
