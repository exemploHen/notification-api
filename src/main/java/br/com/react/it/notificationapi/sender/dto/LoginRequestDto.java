package br.com.react.it.notificationapi.sender.dto;

import java.io.Serializable;

public class LoginRequestDto implements Serializable {
  private static final long serialVersionUID = 97096874533855014L;

  private String login;

  private String password;

  public String getLogin() {
    return login;
  }

  public void setLogin(String login) {
    this.login = login;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }
}
