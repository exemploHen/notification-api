package br.com.react.it.notificationapi.sender.dto;

import java.io.Serializable;
import java.util.List;

public class SendBatchEmailDto implements Serializable{
  private static final long serialVersionUID = 1715840804661753339L;

  private List<SendComplexEmailDto> emails;

  private String origin;

  public List<SendComplexEmailDto> getEmails() {
    return emails;
  }

  public void setEmails(List<SendComplexEmailDto> emails) {
    this.emails = emails;
  }

  public String getOrigin() {
    return origin;
  }

  public void setOrigin(String origin) {
    this.origin = origin;
  }

  @Override
  public String toString() {
    return "SendBatchEmailDto{" +
        "emails=" + emails +
        ", origin='" + origin + '\'' +
        '}';
  }
}
