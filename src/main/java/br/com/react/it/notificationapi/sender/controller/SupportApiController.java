package br.com.react.it.notificationapi.sender.controller;

import br.com.react.it.notificationapi.sender.dto.VersionDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.Serializable;

@Api(value = "Suporte", description = "Responsável por requisições sobre a API")
@RestController
@RequestMapping(value = "/api/support", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public class SupportApiController implements Serializable {

  private static final long serialVersionUID = -2171496331224046761L;

  @Value("${project.name}")
  private String projectName;

  @Value("${project.version}")
  private String projectVersion;

  @ApiOperation(value = "Requisição de versão da API", response = VersionDto.class)
  @GetMapping(value = "/version")
  public VersionDto version() {
    VersionDto responseDto = new VersionDto();
    responseDto.setProjectName(projectName);
    responseDto.setVersion(projectVersion);
    return responseDto;
  }

}
