package br.com.react.it.notificationapi.sender.controller;

import br.com.react.it.notificationapi.common.model.UserEntity;
import br.com.react.it.notificationapi.common.service.UserService;
import br.com.react.it.notificationapi.sender.dto.ResponseDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

@Api(value = "user", description = "Responsável pelo cadastro de usuário")
@RestController
@RequestMapping(value = "/api/user", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public class UserApiController {

  private final UserService userService;

  @Autowired
  public UserApiController(UserService userService) {
    this.userService = userService;
  }

  @PutMapping
  @ApiOperation(value = "Cadastro de usuário", response = ResponseDto.class)
  public ResponseDto prioritary() {
    UserEntity entity = new UserEntity();
    entity.setEmail("rivhenrique@gmail.com");
    entity.setLogin("henrique");
    entity.setStatus(1);
    entity.setApiKey("abcd123");
    entity.setPassword("teste");
    entity = userService.registerUser(entity);
    ResponseDto responseDto = new ResponseDto();
    responseDto.setCode(0);
    responseDto.setMessage("Usuário " + entity.getEmail() + " criado, id: " + entity.getIdUser());
    return responseDto;
  }

  @PostMapping("/login")
  public void login() {
    Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
    System.out.println(authentication);
  }

  @GetMapping("/teste-com-token")
  public void teste() {
    Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
    System.out.println(authentication.getPrincipal());
    System.out.println("Passei pela controller");
  }
}
