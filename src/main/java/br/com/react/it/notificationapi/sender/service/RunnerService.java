package br.com.react.it.notificationapi.sender.service;

import br.com.react.it.notificationapi.common.dto.EmailQueueDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Service;

@Service
public class RunnerService {

  private JmsTemplate jmsTemplate;

  @Autowired
  public RunnerService(JmsTemplate jmsTemplate) {
    this.jmsTemplate = jmsTemplate;
  }

  public void sendToQueue(EmailQueueDto request) {
    jmsTemplate.convertAndSend("mailbox", request);
  }

  public void sendPriorityToQueue(EmailQueueDto request) {
    jmsTemplate.convertAndSend("priority", request);
  }

}
