package br.com.react.it.notificationapi.common.repository;

import br.com.react.it.notificationapi.common.model.BatchNotificationEntity;
import org.springframework.data.repository.CrudRepository;

public interface BatchNotificationRepository extends CrudRepository<BatchNotificationEntity, Long> {
}
