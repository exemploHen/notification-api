package br.com.react.it.notificationapi.sender.service;

import br.com.react.it.notificationapi.common.dto.AttachmentQueueDto;
import br.com.react.it.notificationapi.common.dto.EmailQueueDto;
import br.com.react.it.notificationapi.common.dto.SendSimpleEmailDto;
import br.com.react.it.notificationapi.common.exception.BusinessException;
import br.com.react.it.notificationapi.common.model.BatchNotificationEntity;
import br.com.react.it.notificationapi.common.model.EmailNotificationEntity;
import br.com.react.it.notificationapi.common.service.EmailNotificationService;
import br.com.react.it.notificationapi.sender.dto.AttachmentRequestDto;
import br.com.react.it.notificationapi.sender.dto.InlineRequestDto;
import br.com.react.it.notificationapi.common.dto.InlineQueueDto;
import br.com.react.it.notificationapi.sender.dto.SendComplexEmailDto;
import br.com.react.it.notificationapi.sender.dto.SendBatchEmailDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.Base64;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ManagerService {

  private static final Integer PRIORITARY = 1;

  private RunnerService runnerService;

  private EmailNotificationService emailNotificationService;

  @Value("${mail.limit.priority}")
  private Integer maxQuantityAllowed;

  @Autowired
  public ManagerService(RunnerService runnerService, EmailNotificationService emailNotificationService) {
    this.runnerService = runnerService;
    this.emailNotificationService = emailNotificationService;
  }

  public void sendComplexEmail(SendComplexEmailDto request) {
    EmailNotificationEntity emailNotificationEntity = emailNotificationService.saveDto(request);
    EmailQueueDto emailQueueDto = sendComplexEmailDtoToQueueDto(request, emailNotificationEntity);
    runnerService.sendToQueue(emailQueueDto);
  }

  private EmailQueueDto sendComplexEmailDtoToQueueDto(SendComplexEmailDto request, EmailNotificationEntity entity) {
    EmailQueueDto queueDto = buildEmailQueueDto(entity, request.getEmail(), request.getSubject(), request.getMessage());
    addAttachmentsIfExists(request, queueDto);
    addHtml(request, queueDto);
    addInlinesIfExists(request, queueDto);
    return queueDto;
  }

  private void addHtml(SendComplexEmailDto request, EmailQueueDto queueDto) {
    queueDto.setHtml(request.getHtml());
  }

  private void addAttachmentsIfExists(SendComplexEmailDto request, EmailQueueDto queueDto) {
    if ( request.getAttachments() != null ) {
      List<AttachmentQueueDto> attachmentList = request.getAttachments().stream()
          .map(this::convert).collect(Collectors.toList());
      queueDto.setAttachments(attachmentList);
    }
  }

  private void addInlinesIfExists(SendComplexEmailDto request, EmailQueueDto queueDto) {
    if(request.getInlines() != null) {
      List<InlineQueueDto> inlineQueueDtos = request.getInlines().stream()
          .map(this::convert).collect(Collectors.toList());
      queueDto.setInlines(inlineQueueDtos);
    }
  }

  private InlineQueueDto convert(InlineRequestDto inline) {
    InlineQueueDto inlineQueue = new InlineQueueDto();
    inlineQueue.setInlineName(inline.getInlineName());
    inlineQueue.setInline(base64ToString(inline.getInline()));
    return inlineQueue;
  }

  private AttachmentQueueDto convert(AttachmentRequestDto inline) {
    AttachmentQueueDto attachmentQueueDto = new AttachmentQueueDto();
    attachmentQueueDto.setAttachmentName(inline.getAttachmentName());
    attachmentQueueDto.setAttachment(base64ToString(inline.getAttachment()));
    return attachmentQueueDto;
  }

  private byte[] base64ToString(String base64) {
    return Base64.getDecoder().decode(base64);
  }

  private EmailQueueDto buildEmailQueueDto(EmailNotificationEntity entity, String email, String subject, String message) {
    final EmailQueueDto queueDto = new EmailQueueDto();
    queueDto.setIdEntity(entity.getId());
    queueDto.setRecipient(email);
    queueDto.setSubject(subject);
    queueDto.setMessage(message);
    return queueDto;
  }

  @Async
  public void sendBatchEmail(SendBatchEmailDto request, BatchNotificationEntity batchEntity) {
    for (SendComplexEmailDto email : request.getEmails()) {
      EmailNotificationEntity emailNotificationEntity = emailNotificationService.saveDto(email, batchEntity.getId(), request.getOrigin());
      EmailQueueDto emailQueueDto = sendComplexEmailDtoToQueueDto(email, emailNotificationEntity);
      runnerService.sendToQueue(emailQueueDto);
    }
  }

  private EmailQueueDto sendSimpleEmailDtoToQueueDto(SendSimpleEmailDto request, EmailNotificationEntity entity) {
    EmailQueueDto queueDto = buildEmailQueueDto(entity, request.getEmail(), request.getSubject(), request.getMessage());
    queueDto.setHtml(request.getHtml());
    return queueDto;
  }

  public void sendPrioritaryEmail(SendBatchEmailDto request) {
    verifyQuantityAllowed(request);
    for (SendComplexEmailDto email : request.getEmails()) {
      EmailNotificationEntity emailNotificationEntity = emailNotificationService.saveDto(email, PRIORITARY, request.getOrigin());
      EmailQueueDto emailQueueDto = sendComplexEmailDtoToQueueDto(email, emailNotificationEntity);
      runnerService.sendPriorityToQueue(emailQueueDto);
    }
  }

  private void verifyQuantityAllowed(SendBatchEmailDto request) {
    if (!request.getEmails().isEmpty() && request.getEmails().size() > maxQuantityAllowed ) {
      throw new BusinessException("Quantidade maior que a permitida.");
    }
  }
}
