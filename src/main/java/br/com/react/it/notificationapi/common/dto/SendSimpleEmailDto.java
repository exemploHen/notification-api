package br.com.react.it.notificationapi.common.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

public class SendSimpleEmailDto implements Serializable {
  private static final long serialVersionUID = -5607929571558905008L;

  @ApiModelProperty(name = "Mensagem", required = true)
  @NotNull
  private String message;

  @ApiModelProperty(name = "Email do destinatário", required = true)
  @NotNull
  private String email;

  @ApiModelProperty(name = "Assunto")
  private String subject;

  @ApiModelProperty(name = "HTML")
  private String html;

  @ApiModelProperty(name = "Id externo")
  @JsonProperty(value = "external_id")
  private Object externalId;

  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getSubject() {
    return subject;
  }

  public void setSubject(String subject) {
    this.subject = subject;
  }

  public Object getExternalId() {
    return externalId;
  }

  public void setExternalId(Object externalId) {
    this.externalId = externalId;
  }

  public String getHtml() {
    return html;
  }

  public void setHtml(String html) {
    this.html = html;
  }

  @Override
  public String toString() {
    return "SendSimpleEmailDto{" +
        "message='" + message + '\'' +
        ", email='" + email + '\'' +
        ", subject='" + subject + '\'' +
        ", html='" + html + '\'' +
        ", externalId=" + externalId +
        '}';
  }
}
