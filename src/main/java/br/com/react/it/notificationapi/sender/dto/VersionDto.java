package br.com.react.it.notificationapi.sender.dto;

import java.io.Serializable;

public class VersionDto implements Serializable {

  private static final long serialVersionUID = 8313269267027027650L;

  private String projectName;

  private String version;

  public String getProjectName() {
    return projectName;
  }

  public void setProjectName(String projectName) {
    this.projectName = projectName;
  }

  public String getVersion() {
    return version;
  }

  public void setVersion(String version) {
    this.version = version;
  }

  @Override
  public String toString() {
    return "VersionDto{" +
        "projectName='" + projectName + '\'' +
        ", version='" + version + '\'' +
        '}';
  }
}
