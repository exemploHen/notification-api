package br.com.react.it.notificationapi.common.service;

import br.com.react.it.notificationapi.common.dto.EmailNotificationDto;
import br.com.react.it.notificationapi.common.exception.BusinessException;
import br.com.react.it.notificationapi.common.model.EmailNotificationEntity;
import br.com.react.it.notificationapi.common.repository.EmailNotificationRepository;
import br.com.react.it.notificationapi.sender.authentication.ApiUserDetails;
import br.com.react.it.notificationapi.sender.dto.CallbackEventDto;
import br.com.react.it.notificationapi.sender.dto.SearchByJsonRequestDto;
import br.com.react.it.notificationapi.sender.dto.SendComplexEmailDto;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class EmailNotificationService {

  private static final Integer CREATED = 0;
  private static final Integer SUCCESS = 1;
  private static final Integer PROCESSING = 2;
  private static final Integer FAILED = 3;

  private EmailNotificationRepository repository;

  @Autowired
  public EmailNotificationService(EmailNotificationRepository repository) {
    this.repository = repository;
  }

  public EmailNotificationEntity saveDto(SendComplexEmailDto dto) {
    EmailNotificationEntity entity = getEmailNotificationEntity(dto.getOrigin(), dto.getExternalId(), dto.getEmail(), dto.getSubject());
    return repository.save(entity);
  }

  public EmailNotificationEntity saveDto(SendComplexEmailDto dto, Long batchId, String requestOrigin) {
    EmailNotificationEntity entity = getEmailNotificationEntity(requestOrigin, dto.getExternalId(), dto.getEmail(), dto.getSubject());
    entity.setIdBatch(batchId);
    return repository.save(entity);
  }

  private EmailNotificationEntity getEmailNotificationEntity(String origin, Object externalId, String email, String subject) {
    EmailNotificationEntity entity = new EmailNotificationEntity();
    entity.setDateCreated(LocalDateTime.now());
    entity.setStatus(CREATED);
    entity.setRequestOrigin(origin);
    entity.setExternalId(new ObjectMapper().convertValue(externalId, Map.class));
    entity.setRecipient(email);
    entity.setSubject(subject);
    entity.setIdUser(retrieveUserFromAuthentication());
    return entity;
  }

  public EmailNotificationEntity saveDto(SendComplexEmailDto dto, Integer prioritary, String requestOrigin) {
    EmailNotificationEntity entity = getEmailNotificationEntity(requestOrigin, dto.getExternalId(), dto.getEmail(), dto.getSubject());
    entity.setPrioritary(prioritary);
    return repository.save(entity);
  }

  public EmailNotificationDto findOne(Long id) throws BusinessException {
    final EmailNotificationEntity entity = repository.findById(id).orElseThrow(() -> new BusinessException("Entidade não encontrada"));
    return new EmailNotificationDto(entity);
  }

  public List<EmailNotificationDto> findByIdBatch(Long idBatch) throws BusinessException {
    final List<EmailNotificationEntity> entityList = repository.findByIdBatch(idBatch);
    verifyEntityList(entityList);
    return entityListToDtoList(entityList);
  }

  public List findByJson(SearchByJsonRequestDto request) throws BusinessException {
    final Map map = new ObjectMapper().convertValue(request.getExternalId(), Map.class);
    final Long idUser = retrieveUserFromAuthentication();
    final List entityList = repository.searchByJsonProperty(idUser, map);
    verifyEntityList(entityList);
    return entityListToDtoList(entityList);
  }

  private Long retrieveUserFromAuthentication() {
    return ((ApiUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getId();
  }

  private List<EmailNotificationDto> entityListToDtoList(List<EmailNotificationEntity> entityList) {
    return entityList.stream().map(EmailNotificationDto::new).collect(Collectors.toList());
  }

  public void markFailedEmail(Long idEntity) throws BusinessException {
    updateEntityStatus(idEntity, FAILED);
  }

  public void markProcessingEmail(Long idEntity) throws BusinessException {
    updateEntityStatus(idEntity, PROCESSING);
  }

  private EmailNotificationEntity markSuccessEmail(Long idEntity) throws BusinessException {
    return updateEntityStatus(idEntity, SUCCESS);
  }

  private EmailNotificationEntity updateEntityStatus(Long idEntity, Integer status) throws BusinessException {
    EmailNotificationEntity entity = repository.findById(idEntity).orElseThrow(() -> new BusinessException("Entidade não encontrada."));
    entity.setStatus(status);
    entity.setId(idEntity);
    entity.setDateProcessed(LocalDateTime.now());
    return repository.save(entity);
  }

  public void updateSuccessfulMailSent(Long idEntity, String idMailgun) throws BusinessException {
    final EmailNotificationEntity entity = markSuccessEmail(idEntity);
    entity.setIdMailgun(formatIdMailgun(idMailgun));
    repository.save(entity);
  }

  private String formatIdMailgun(String idMailgun) {
    return idMailgun.replaceAll("[<>]", "");
  }

  public List<EmailNotificationDto> findByRequestOrigin(String origin) throws BusinessException {
    final Long idUser = retrieveUserFromAuthentication();
    final List<EmailNotificationEntity> entityList = repository.findByRequestOriginAndIdUser(origin, idUser);
    verifyEntityList(entityList);
    return entityListToDtoList(entityList);
  }

  public void updateMailgunCallback(CallbackEventDto request) throws BusinessException {
    final EmailNotificationEntity entity = repository.findByIdMailgun(request.getEventData().getMessage().getHeaders().getMessageId());
    verifyEntity(entity);
    entity.setMailgunStatus(request.getEventData().getEvent());
    repository.save(entity);
  }

  private void verifyEntity(EmailNotificationEntity entity) throws BusinessException {
    if (entity == null) {
      throw new BusinessException("Email não encontrado.");
    }
  }

  private void verifyEntityList(List<EmailNotificationEntity> entityList) throws BusinessException {
    if (entityList == null || entityList.isEmpty()) {
      throw new BusinessException("Emails não encontrado.");
    }
  }
}
