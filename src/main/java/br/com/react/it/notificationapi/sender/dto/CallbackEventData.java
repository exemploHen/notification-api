package br.com.react.it.notificationapi.sender.dto;

import java.io.Serializable;

public class CallbackEventData implements Serializable{
  private static final long serialVersionUID = -4003252238873129784L;

  private String event;
  private CallbackMessageDetailsDto message;

  public String getEvent() {
    return event;
  }

  public void setEvent(String event) {
    this.event = event;
  }

  public CallbackMessageDetailsDto getMessage() {
    return message;
  }

  public void setMessage(CallbackMessageDetailsDto message) {
    this.message = message;
  }
}
