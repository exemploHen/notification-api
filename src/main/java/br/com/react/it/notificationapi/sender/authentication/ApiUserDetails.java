package br.com.react.it.notificationapi.sender.authentication;

import br.com.react.it.notificationapi.common.model.UserEntity;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.List;

public class ApiUserDetails implements UserDetails {

  private static final long serialVersionUID = 288442855224510334L;

  private UserEntity userEntity;

  private String roles;

  ApiUserDetails(UserEntity userEntity, List<String> roles) {
    this.userEntity = userEntity;
    this.roles = String.join(",", roles);
  }

  @Override
  public Collection<? extends GrantedAuthority> getAuthorities() {
    return AuthorityUtils.commaSeparatedStringToAuthorityList(roles);
  }

  public Long getId() {
    return userEntity.getIdUser();
  }

  public String getKey() {
    return userEntity.getApiKey();
  }

  @Override
  public String getPassword() {
    return userEntity.getPassword();
  }

  @Override
  public String getUsername() {
    return userEntity.getEmail();
  }

  @Override
  public boolean isAccountNonExpired() {
    return userEntity.getStatus() == 1;
  }

  @Override
  public boolean isAccountNonLocked() {
    return userEntity.getStatus() == 1;
  }

  @Override
  public boolean isCredentialsNonExpired() {
    return userEntity.getStatus() == 1;
  }

  @Override
  public boolean isEnabled() {
    return userEntity.getStatus() == 1;
  }
}
