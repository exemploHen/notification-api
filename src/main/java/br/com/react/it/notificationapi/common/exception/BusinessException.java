package br.com.react.it.notificationapi.common.exception;

public class BusinessException extends RuntimeException {
  private static final long serialVersionUID = -6766547309549288187L;

  public BusinessException(String s) {
    super(s);
  }
}
