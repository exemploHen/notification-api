package br.com.react.it.notificationapi.sender.controller;

import br.com.react.it.notificationapi.sender.dto.CallbackEventDto;
import br.com.react.it.notificationapi.common.dto.ErrorResponseDto;
import br.com.react.it.notificationapi.common.exception.BusinessException;
import br.com.react.it.notificationapi.common.exception.MailgunException;
import br.com.react.it.notificationapi.common.service.EmailNotificationService;
import br.com.react.it.notificationapi.receiver.service.mailgun.MailgunService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.io.Serializable;

@Api(value = "callback", description = "Responsável por escutar eventos da api do mailgun")
@RestController
@RequestMapping(value = "/api/callback", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public class CallbackApiController implements Serializable {
  private static final long serialVersionUID = 8544872291534025758L;

  private EmailNotificationService emailNotificationService;

  private MailgunService mailgunService;

  @Autowired
  public CallbackApiController(EmailNotificationService emailNotificationService, MailgunService mailgunService) {
    this.emailNotificationService = emailNotificationService;
    this.mailgunService = mailgunService;
  }

  @PostMapping(value = "/event")
  public void eventCallback(@Valid @RequestBody CallbackEventDto request) throws Exception {
    mailgunService.verifyCallbackRequest(request.getSignature());
    emailNotificationService.updateMailgunCallback(request);
  }

  @ExceptionHandler(BusinessException.class)
  public ResponseEntity<ErrorResponseDto> handleBusinessException(BusinessException e) {
    return new ResponseEntity<>(
        new ErrorResponseDto(HttpStatus.OK.value(),e.getMessage()), new HttpHeaders(), HttpStatus.OK);
  }

  @ExceptionHandler(MailgunException.class)
  public ResponseEntity<ErrorResponseDto> handleMailgunException(MailgunException e) {
    return new ResponseEntity<>(
        new ErrorResponseDto(HttpStatus.UNAUTHORIZED.value(),e.getMessage()), new HttpHeaders(), HttpStatus.UNAUTHORIZED);
  }

  @ExceptionHandler(Exception.class)
  public ResponseEntity<ErrorResponseDto> handleException(Exception e) {
    return new ResponseEntity<>(
        new ErrorResponseDto(HttpStatus.INTERNAL_SERVER_ERROR.value(),e.getMessage()), new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR);
  }
}
