package br.com.react.it.notificationapi.sender.dto;

import java.io.Serializable;

public class MailgunSignature implements Serializable {
  private static final long serialVersionUID = -1524299091139959329L;

  private String timestamp;
  private String token;
  private String signature;

  public String getTimestamp() {
    return timestamp;
  }

  public void setTimestamp(String timestamp) {
    this.timestamp = timestamp;
  }

  public String getToken() {
    return token;
  }

  public void setToken(String token) {
    this.token = token;
  }

  public String getSignature() {
    return signature;
  }

  public void setSignature(String signature) {
    this.signature = signature;
  }
}
