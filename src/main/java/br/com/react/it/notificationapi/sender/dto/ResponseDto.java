package br.com.react.it.notificationapi.sender.dto;

import java.io.Serializable;

public class ResponseDto implements Serializable {
  private static final long serialVersionUID = -262307175067553148L;

  private String message;

  private Integer code;

  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }

  public Integer getCode() {
    return code;
  }

  public void setCode(Integer code) {
    this.code = code;
  }

  @Override
  public String toString() {
    return "ResponseDto{" +
        "message='" + message + '\'' +
        ", code=" + code +
        '}';
  }

  public void configureAsSucess() {
    setMessage("Sucess!");
    setCode(0);
  }
}
