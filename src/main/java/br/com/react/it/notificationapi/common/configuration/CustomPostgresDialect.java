package br.com.react.it.notificationapi.common.configuration;

import org.hibernate.dialect.PostgreSQL94Dialect;

import java.sql.Types;

public class CustomPostgresDialect extends PostgreSQL94Dialect {

  public CustomPostgresDialect() {
    this.registerColumnType(Types.JAVA_OBJECT, "jsonb");
  }
}
