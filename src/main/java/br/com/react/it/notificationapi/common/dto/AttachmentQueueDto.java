package br.com.react.it.notificationapi.common.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class AttachmentQueueDto implements Serializable {

  private static final long serialVersionUID = -9107452579129396853L;
  private byte[] attachment;

  @JsonProperty(value = "attachment_name")
  private String attachmentName;

  public byte[] getAttachment() {
    return attachment;
  }

  public void setAttachment(byte[] attachment) {
    this.attachment = attachment;
  }

  public String getAttachmentName() {
    return attachmentName;
  }

  public void setAttachmentName(String attachmentName) {
    this.attachmentName = attachmentName;
  }
}
