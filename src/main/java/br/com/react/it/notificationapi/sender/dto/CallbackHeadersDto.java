package br.com.react.it.notificationapi.sender.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class CallbackHeadersDto implements Serializable {
  private static final long serialVersionUID = 4543974030833531346L;

  @JsonProperty(value = "message-id")
  private String messageId;

  public String getMessageId() {
    return messageId;
  }

  public void setMessageId(String messageId) {
    this.messageId = messageId;
  }
}
