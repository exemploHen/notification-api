package br.com.react.it.notificationapi.receiver.service.mailgun;

import br.com.react.it.notificationapi.common.dto.AttachmentQueueDto;
import br.com.react.it.notificationapi.common.dto.EmailQueueDto;
import br.com.react.it.notificationapi.common.dto.InlineQueueDto;
import br.com.react.it.notificationapi.common.exception.MailgunException;
import br.com.react.it.notificationapi.receiver.dto.MailgunResponse;
import br.com.react.it.notificationapi.sender.dto.MailgunSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URI;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Base64;

@Service
public class MailgunService {

  private RestTemplate restTemplate;

  @Value("${mailgun.url.base}")
  private String MAILGUN_URL_BASE;

  @Value("${mailgun.api.key}")
  private String MAILGUN_API_KEY;

  @Value("${mailgun.domain}")
  private String MAILGUN_DOMAIN_NAME;

  private final static String HMAC_SHA_256 = "HmacSHA256";

  @Autowired
  public MailgunService(RestTemplate restTemplate) {
    this.restTemplate = restTemplate;
  }

  public MailgunResponse sendMailGunEmail(EmailQueueDto emailDto) {
    MailgunResponse response;

    try {
      final URI url = UriComponentsBuilder.fromUriString(MAILGUN_URL_BASE + "/" + MAILGUN_DOMAIN_NAME + "/messages").build().toUri();

      HttpHeaders headers = new HttpHeaders();
      headers.setContentType(MediaType.MULTIPART_FORM_DATA);
      addAutheticationHeader(headers);

      MultiValueMap<String, Object> map = new LinkedMultiValueMap<>();
      String from = emailDto.getFrom() == null ? String.format("No Reply <no-reply@%s>", MAILGUN_DOMAIN_NAME) : emailDto.getFrom();
      addTextParameterIfExists(map, "from", from);
      addTextParameterIfExists(map, "to", emailDto.getRecipient());
      addTextParameterIfExists(map, "subject", emailDto.getSubject());
      addTextParameterIfExists(map, "text", emailDto.getMessage(), "\nSe deseja parar de receber emails desse tipo, clique <a href=\"%unsubscribe_url%\">AQUI</a><br>");
      addTextParameterIfExists(map, "html", emailDto.getHtml(), "<br> Se deseja parar de receber emails desse tipo, clique <a href=\"%unsubscribe_url%\">AQUI</a><br>");
      addTextParameterIfExists(map, "o:deliverytime", formatDeliveryTime(emailDto.getDeliveryTime()));

      if(emailDto.getAttachments() != null) {
        emailDto.getAttachments()
            .forEach(attachment -> map.add("attachment", buildFilesFromAttachments(attachment)));
      }

      if(emailDto.getInlines() != null) {
        emailDto.getInlines()
            .forEach(inline -> map.add("inline", buildFilesFromInlines(inline)));
      }

      HttpEntity<MultiValueMap<String, Object>> request = new HttpEntity<>(map, headers);

      response = restTemplate.postForEntity(url, request, MailgunResponse.class).getBody();
    } catch ( Exception e ) {
      e.printStackTrace();
      throw new MailgunException(e.getMessage());
    }
    deleteFile(emailDto);
    return response;
  }

  private void addTextParameterIfExists(MultiValueMap<String, Object> map, String key, String message, String... extra) {
    if( message != null ){
      StringBuilder messageBuilder = new StringBuilder(message);
      for(String add : extra) {
        messageBuilder.append(add);
      }
      message = messageBuilder.toString();
      map.add(key, message);
    }
  }

  private String formatDeliveryTime(LocalDateTime deliveryTime) {
    if (deliveryTime == null) {
      return null;
    }
    final ZonedDateTime zonedDateTime = ZonedDateTime.of(deliveryTime, ZoneId.of("Brazil/East"));
    return zonedDateTime.format(DateTimeFormatter.RFC_1123_DATE_TIME);
  }

  private void deleteFile(EmailQueueDto emailDto) {
    if ( emailDto.getAttachments() != null ) {
      emailDto.getAttachments().stream()
          .map(attachment -> new File(attachment.getAttachmentName()))
          .forEach(File::delete);
    }
    if(emailDto.getInlines() != null) {
      emailDto.getInlines().stream()
          .map(inline -> new File(inline.getInlineName()))
          .forEach(File::delete);
    }
  }

  private void addAutheticationHeader(HttpHeaders headers) {
    String userCredentials = "api:" + MAILGUN_API_KEY;
    String basicAuth = "Basic " + new String(Base64.getEncoder().encode(userCredentials.getBytes()));
    headers.set("Authorization", basicAuth);
  }

  private FileSystemResource buildFilesFromInlines(InlineQueueDto inline) {
    try {
      return buildFile(inline.getInlineName(), inline.getInline());
    } catch (Exception e) {
      System.out.println("Inline deu errado: " + inline.getInlineName());
      e.printStackTrace();
      return null;
    }
  }

  private FileSystemResource buildFilesFromAttachments(AttachmentQueueDto attachments) {
    try {
      return buildFile(attachments.getAttachmentName(), attachments.getAttachment());
    } catch (Exception e) {
      System.out.println("Atalho deu errado: " + attachments.getAttachmentName());
      e.printStackTrace();
      return null;
    }
  }

  private FileSystemResource buildFile(String attachmentName, byte[] attachment) throws IOException {
    File file = new File(attachmentName);
    FileOutputStream fos = new FileOutputStream(file);
    fos.write(attachment);
    fos.flush();
    fos.close();
    return new FileSystemResource(file);
  }

  public void verifyCallbackRequest(MailgunSignature request) throws Exception {
    try {
      final String concatenated = request.getTimestamp() + request.getToken();
      Mac hMac = Mac.getInstance(HMAC_SHA_256);
      hMac.init(new SecretKeySpec(MAILGUN_API_KEY.getBytes(), HMAC_SHA_256));
      final byte[] encodedSignature = hMac.doFinal(concatenated.getBytes());
      final String hashToString = convertHashToString(encodedSignature);
      if ( !hashToString.equals(request.getSignature()) ) {
        throw new MailgunException("Signature doesn't match.");
      }
    } catch (NoSuchAlgorithmException | InvalidKeyException e) {
      e.printStackTrace();
      throw new Exception("Erro na verificação de assinatura.");
    } catch(Exception e) {
      e.printStackTrace();
    }

  }

  private String convertHashToString(byte[] encodedSignature) {
    StringBuilder hash = new StringBuilder();
    for (byte b : encodedSignature) {
      String hex = Integer.toHexString(0xFF & b);
      if (hex.length() == 1) {
        hash.append('0');
      }
      hash.append(hex);
    }
    return hash.toString();
  }
}
