package br.com.react.it.notificationapi.receiver.service;

import br.com.react.it.notificationapi.receiver.dto.MailgunResponse;
import br.com.react.it.notificationapi.receiver.service.mailgun.MailgunService;
import br.com.react.it.notificationapi.common.dto.EmailQueueDto;
import br.com.react.it.notificationapi.common.exception.BusinessException;
import br.com.react.it.notificationapi.common.service.EmailNotificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDateTime;

@Service
public class MailService {

  private MailgunService mailgunService;

  private EmailNotificationService emailNotificationService;

  @Value("${mail.time.initial}")
  private Integer minimalTimeAllowed;

  @Value("${mail.time.final}")
  private Integer maximalTimeAllowed;

  @Autowired
  public MailService(MailgunService mailgunService, EmailNotificationService emailNotificationService) {
    this.mailgunService = mailgunService;
    this.emailNotificationService = emailNotificationService;
  }

  @Transactional
  public void sendEmail(EmailQueueDto queueDto) throws BusinessException {
    verifyTimeIsAllowed(queueDto);
    processEmail(queueDto);
  }

  @Transactional
  public void sendPrioritaryEmail(EmailQueueDto queueDto) throws BusinessException {
    processEmail(queueDto);
  }

  private void processEmail(EmailQueueDto queueDto) {
    emailNotificationService.markProcessingEmail(queueDto.getIdEntity());
    final MailgunResponse response = mailgunService.sendMailGunEmail(queueDto);
    if (response != null) {
      emailNotificationService.updateSuccessfulMailSent(queueDto.getIdEntity(), response.getId());
    } else {
      emailNotificationService.markFailedEmail(queueDto.getIdEntity());
    }
  }

  private void verifyTimeIsAllowed(EmailQueueDto queueDto) {
    LocalDateTime now = LocalDateTime.now();
    LocalDateTime deliveryTime = null;

    if ( now.getHour() < minimalTimeAllowed ) {
      deliveryTime = now.withHour(minimalTimeAllowed);
    } else if ( now.getHour() >= maximalTimeAllowed ) {
      deliveryTime = now.plusDays(1).withHour(minimalTimeAllowed);
    }
    queueDto.setDeliveryTime(deliveryTime);
  }

}
