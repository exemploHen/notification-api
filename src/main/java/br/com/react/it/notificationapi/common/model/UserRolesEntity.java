package br.com.react.it.notificationapi.common.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(schema = "apicommunication", name = "user_roles")
public class UserRolesEntity implements Serializable {
  private static final long serialVersionUID = -6798677470086765441L;

  @Id
  @Column(name = "id_user_roles")
  private Long idUserRoles;

  @Column(name = "id_user")
  private Long idUser;

  @Column(name = "role")
  private String role;

  public Long getIdUserRoles() {
    return idUserRoles;
  }

  public void setIdUserRoles(Long idUserRoles) {
    this.idUserRoles = idUserRoles;
  }

  public Long getIdUser() {
    return idUser;
  }

  public void setIdUser(Long idUser) {
    this.idUser = idUser;
  }

  public String getRole() {
    return role;
  }

  public void setRole(String role) {
    this.role = role;
  }

  @Override
  public String toString() {
    return "UserRolesEntity{" +
        "idUserRoles=" + idUserRoles +
        ", idUser=" + idUser +
        ", role='" + role + '\'' +
        '}';
  }
}
