package br.com.react.it.notificationapi.common.model;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@Entity
@Table(schema = "apicommunication", name = "batch_notification")
public class BatchNotificationEntity implements Serializable {
  private static final long serialVersionUID = 5846295567610376891L;

  @Id
  @SequenceGenerator(name="seq_id_batch_notification_generator", sequenceName="apicommunication.seq_id_batch_notification",allocationSize = 1)
  @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="seq_id_batch_notification_generator")
  @Column(name = "id_batch")
  private Long id;

  @Column(name = "date_created")
  private LocalDateTime dateCreated;

  @Column(name = "status")
  private Integer status;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public LocalDateTime getDateCreated() {
    return dateCreated;
  }

  public void setDateCreated(LocalDateTime dateCreated) {
    this.dateCreated = dateCreated;
  }

  public Integer getStatus() {
    return status;
  }

  public void setStatus(Integer status) {
    this.status = status;
  }
}
