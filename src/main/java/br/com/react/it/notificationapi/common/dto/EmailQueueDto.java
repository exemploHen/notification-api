package br.com.react.it.notificationapi.common.dto;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

public class EmailQueueDto implements Serializable {

  private static final long serialVersionUID = 1324887101402475716L;

  private Long idEntity;

  private String subject;

  private String recipient;

  private String message;

  private String html;

  private List<AttachmentQueueDto> attachments;

  private LocalDateTime deliveryTime;

  private List<InlineQueueDto> inlines;

  private String from;

  public Long getIdEntity() {
    return idEntity;
  }

  public void setIdEntity(Long idEntity) {
    this.idEntity = idEntity;
  }

  public String getSubject() {
    return subject;
  }

  public void setSubject(String subject) {
    this.subject = subject;
  }

  public String getRecipient() {
    return recipient;
  }

  public void setRecipient(String recipient) {
    this.recipient = recipient;
  }

  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }

  public String getHtml() {
    return html;
  }

  public void setHtml(String html) {
    this.html = html;
  }

  public List<AttachmentQueueDto> getAttachments() {
    return attachments;
  }

  public void setAttachments(List<AttachmentQueueDto> attachments) {
    this.attachments = attachments;
  }

  public LocalDateTime getDeliveryTime() {
    return deliveryTime;
  }

  public void setDeliveryTime(LocalDateTime deliveryTime) {
    this.deliveryTime = deliveryTime;
  }

  public List<InlineQueueDto> getInlines() {
    return inlines;
  }

  public void setInlines(List<InlineQueueDto> inlines) {
    this.inlines = inlines;
  }

  public String getFrom() {
    return from;
  }

  public void setFrom(String from) {
    this.from = from;
  }

  @Override
  public String toString() {
    return "EmailQueueDto{" +
        "idEntity=" + idEntity +
        ", subject='" + subject + '\'' +
        ", recipient='" + recipient + '\'' +
        ", message='" + message + '\'' +
        ", html='" + html + '\'' +
        ", deliveryTime=" + deliveryTime +
        ", inlines=" + inlines +
        ", from='" + from + '\'' +
        '}';
  }
}
