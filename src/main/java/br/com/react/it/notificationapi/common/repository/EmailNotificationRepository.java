package br.com.react.it.notificationapi.common.repository;

import br.com.react.it.notificationapi.common.model.EmailNotificationEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EmailNotificationRepository extends CrudRepository<EmailNotificationEntity, Long>, CustomEmailNotificationRepository {

  List<EmailNotificationEntity> findByIdBatch(Long idBatch);

  List<EmailNotificationEntity> findByRequestOriginAndIdUser(String requestOrigin, Long idUser);

  EmailNotificationEntity findByIdMailgun(String messageId);
}
