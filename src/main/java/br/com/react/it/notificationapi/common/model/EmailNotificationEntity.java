package br.com.react.it.notificationapi.common.model;

import br.com.react.it.notificationapi.common.configuration.JsonDataType;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Map;

@Entity
@Table(schema="APICOMMUNICATION", name = "EMAIL_NOTIFICATION")
@TypeDef(name = "JsonDataType", typeClass = JsonDataType.class)
public class EmailNotificationEntity implements Serializable {

  private static final long serialVersionUID = 4914244574348790671L;

  @Id
  @SequenceGenerator(name="seq_id_email_notification_generator", sequenceName="apicommunication.seq_id_email_notification",allocationSize = 1)
  @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="seq_id_email_notification_generator")
  @Column(name = "id_email_notification")
  private Long id;

  @Type(type = "JsonDataType")
  @Column(name = "external_id")
  private Map<String, Object> externalId;

  @Column(name = "date_created")
  private LocalDateTime dateCreated;

  @Column(name = "date_processed")
  private LocalDateTime dateProcessed;

  @Column(name = "status")
  private Integer status;

  @Column(name = "prioritary")
  private Integer prioritary;

  @Column(name = "request_origin")
  private String requestOrigin;

  @Column(name = "recipient")
  private String recipient;

  @Column(name = "subject")
  private String subject;

  @Column(name = "id_mailgun")
  private String idMailgun;

  @Column(name = "mailgun_status")
  private String mailgunStatus;

  @Column(name = "id_batch")
  private Long idBatch;

  @Column(name = "id_User")
  private Long idUser;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Map<String, Object> getExternalId() {
    return externalId;
  }

  public void setExternalId(Map<String, Object> externalId) {
    this.externalId = externalId;
  }

  public LocalDateTime getDateCreated() {
    return dateCreated;
  }

  public void setDateCreated(LocalDateTime dateCreated) {
    this.dateCreated = dateCreated;
  }

  public LocalDateTime getDateProcessed() {
    return dateProcessed;
  }

  public void setDateProcessed(LocalDateTime dateProcessed) {
    this.dateProcessed = dateProcessed;
  }

  public Integer getStatus() {
    return status;
  }

  public void setStatus(Integer status) {
    this.status = status;
  }

  public Integer getPrioritary() {
    return prioritary;
  }

  public void setPrioritary(Integer prioritary) {
    this.prioritary = prioritary;
  }

  public String getRequestOrigin() {
    return requestOrigin;
  }

  public void setRequestOrigin(String requestOrigin) {
    this.requestOrigin = requestOrigin;
  }

  public String getRecipient() {
    return recipient;
  }

  public void setRecipient(String recipient) {
    this.recipient = recipient;
  }

  public String getSubject() {
    return subject;
  }

  public void setSubject(String subject) {
    this.subject = subject;
  }

  public String getIdMailgun() {
    return idMailgun;
  }

  public void setIdMailgun(String idMailgun) {
    this.idMailgun = idMailgun;
  }

  public String getMailgunStatus() {
    return mailgunStatus;
  }

  public void setMailgunStatus(String mailgunStatus) {
    this.mailgunStatus = mailgunStatus;
  }

  public Long getIdBatch() {
    return idBatch;
  }

  public void setIdBatch(Long idBatch) {
    this.idBatch = idBatch;
  }

  public Long getIdUser() {
    return idUser;
  }

  public void setIdUser(Long idUser) {
    this.idUser = idUser;
  }

  @Override
  public String toString() {
    return "EmailNotificationEntity{" +
        "id=" + id +
        ", externalId=" + externalId +
        ", dateCreated=" + dateCreated +
        ", dateProcessed=" + dateProcessed +
        ", status=" + status +
        ", prioritary=" + prioritary +
        ", requestOrigin='" + requestOrigin + '\'' +
        ", recipient='" + recipient + '\'' +
        ", subject='" + subject + '\'' +
        ", idMailgun='" + idMailgun + '\'' +
        ", mailgunStatus='" + mailgunStatus + '\'' +
        ", idBatch=" + idBatch +
        ", idUser=" + idUser +
        '}';
  }
}
