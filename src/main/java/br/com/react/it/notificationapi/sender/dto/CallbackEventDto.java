package br.com.react.it.notificationapi.sender.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class CallbackEventDto implements Serializable {
  private static final long serialVersionUID = 3927198928459435614L;

  @JsonProperty(value = "event-data")
  private CallbackEventData eventData;

  private MailgunSignature signature;

  public CallbackEventData getEventData() {
    return eventData;
  }

  public void setEventData(CallbackEventData eventData) {
    this.eventData = eventData;
  }

  public MailgunSignature getSignature() {
    return signature;
  }

  public void setSignature(MailgunSignature signature) {
    this.signature = signature;
  }
}
