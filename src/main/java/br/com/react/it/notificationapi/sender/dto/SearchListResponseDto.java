package br.com.react.it.notificationapi.sender.dto;

import br.com.react.it.notificationapi.common.dto.EmailNotificationDto;

import java.util.List;

public class SearchListResponseDto extends ResponseDto{
  private static final long serialVersionUID = -262307175067553148L;

  private List<EmailNotificationDto> emails;

  public List<EmailNotificationDto> getEmails() {
    return emails;
  }

  public void setEmails(List<EmailNotificationDto> emails) {
    this.emails = emails;
  }

  @Override
  public String toString() {
    return "ResponseDto{" +
        "message='" + getMessage() + '\'' +
        ", code=" + getCode() +
        ", emails=" + emails +
        '}';
  }
}
