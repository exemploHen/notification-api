package br.com.react.it.notificationapi.sender.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class BathResponseDto extends ResponseDto {
  private static final long serialVersionUID = 2126867556950112830L;

  @JsonProperty(value = "id_batch")
  private Integer idBatch;

  public Integer getIdBatch() {
    return idBatch;
  }

  public void setIdBatch(Integer idBatch) {
    this.idBatch = idBatch;
  }
}
