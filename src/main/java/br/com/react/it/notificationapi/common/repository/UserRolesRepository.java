package br.com.react.it.notificationapi.common.repository;

import br.com.react.it.notificationapi.common.model.UserRolesEntity;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface UserRolesRepository extends CrudRepository<UserRolesEntity, Long> {

  List<UserRolesEntity> findByIdUser(Long idUser);
}
