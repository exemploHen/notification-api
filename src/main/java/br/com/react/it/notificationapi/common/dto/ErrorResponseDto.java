package br.com.react.it.notificationapi.common.dto;

import org.springframework.http.HttpStatus;

import java.io.Serializable;

public class ErrorResponseDto implements Serializable {
  private static final long serialVersionUID = -1093438285808886275L;

  private String message;
  private Integer code;

  public ErrorResponseDto(Integer code, String message) {
    this.message = message;
    this.code = code;
  }

  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }

  public Integer getCode() {
    return code;
  }

  public void setCode(Integer code) {
    this.code = code;
  }
}
