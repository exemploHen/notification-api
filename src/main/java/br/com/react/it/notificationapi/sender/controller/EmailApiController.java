package br.com.react.it.notificationapi.sender.controller;

import br.com.react.it.notificationapi.common.dto.EmailNotificationDto;
import br.com.react.it.notificationapi.common.dto.ErrorResponseDto;
import br.com.react.it.notificationapi.common.exception.BusinessException;
import br.com.react.it.notificationapi.common.model.BatchNotificationEntity;
import br.com.react.it.notificationapi.common.service.BatchNotificationService;
import br.com.react.it.notificationapi.common.service.EmailNotificationService;
import br.com.react.it.notificationapi.sender.dto.*;
import br.com.react.it.notificationapi.sender.service.ManagerService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.io.Serializable;
import java.util.List;

@Api(value = "email", description = "Responsável pelo envio e busca de emails")
@RestController
@RequestMapping(value = "/api/email", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public class EmailApiController implements Serializable {
  private static final long serialVersionUID = 7537900220506961773L;

  private ManagerService managerService;

  private EmailNotificationService emailNotificationService;

  private BatchNotificationService batchNotificationService;

  @Autowired
  public EmailApiController(ManagerService managerService, EmailNotificationService emailNotificationService, BatchNotificationService batchNotificationService) {
    this.managerService = managerService;
    this.emailNotificationService = emailNotificationService;
    this.batchNotificationService = batchNotificationService;
  }

  @PostMapping("/complex")
  @ApiOperation(value = "Envio de email com anexo e html", response = ResponseDto.class)
  public ResponseDto complexEmail(@Valid @RequestBody SendComplexEmailDto request) {
    managerService.sendComplexEmail(request);
    ResponseDto responseDto = new ResponseDto();
    responseDto.configureAsSucess();
    return responseDto;
  }

  @PostMapping("/batch")
  @ApiOperation(value = "Envio de email em lote", response = ResponseDto.class)
  public BathResponseDto batchEmail(@Valid @RequestBody SendBatchEmailDto request) {
    final BatchNotificationEntity batchEntity = batchNotificationService.createNewBatch();
    managerService.sendBatchEmail(request, batchEntity);
    BathResponseDto responseDto = new BathResponseDto();
    responseDto.configureAsSucess();
    responseDto.setIdBatch(batchEntity.getId().intValue());
    return responseDto;
  }

  @PostMapping("/prioritary")
  @ApiOperation(value = "Envio de email prioritário", response = ResponseDto.class)
  public BathResponseDto prioritary(@Valid @RequestBody SendBatchEmailDto request) {
    managerService.sendPrioritaryEmail(request);
    BathResponseDto responseDto = new BathResponseDto();
    responseDto.setCode(0);
    responseDto.setMessage("Sucesso! Email enviado!");
    return responseDto;
  }

  @PostMapping("/json")
  @ApiOperation(value = "Pesquisa por id_externo", response = SearchListResponseDto.class)
  public SearchListResponseDto searchByJson(@Valid @RequestBody SearchByJsonRequestDto request) throws BusinessException {
    List entities = emailNotificationService.findByJson(request);
    return buildSearchListResponse(entities);
  }

  @GetMapping("/id/{id}")
  @ApiOperation(value = "Pesquisa por id", response = SearchOneResponseDto.class)
  public SearchOneResponseDto searchById(@Valid @PathVariable Long id) throws BusinessException {
    EmailNotificationDto dto = emailNotificationService.findOne(id);
    return buildSearchOneResponse(dto);
  }

  @GetMapping("/batch/{idBatch}")
  @ApiOperation(value = "Pesquisa por Lote", response = SearchListResponseDto.class)
  public SearchListResponseDto searchByIdBatch(@Valid @PathVariable Long idBatch) throws BusinessException {
    List<EmailNotificationDto> entities = emailNotificationService.findByIdBatch(idBatch);
    return buildSearchListResponse(entities);
  }

  @GetMapping("/origin/{origin}")
  @ApiOperation(value = "Pesquisa por origin", response = SearchListResponseDto.class)
  public SearchListResponseDto searchByIdBatch(@Valid @PathVariable String origin) throws BusinessException {
    List<EmailNotificationDto> entities = emailNotificationService.findByRequestOrigin(origin);
    return buildSearchListResponse(entities);
  }

  private SearchOneResponseDto buildSearchOneResponse(EmailNotificationDto dto) {
    SearchOneResponseDto responseDto = new SearchOneResponseDto();
    responseDto.setEmail(dto);
    responseDto.configureAsSucess();
    return responseDto;
  }

  private SearchListResponseDto buildSearchListResponse(List<EmailNotificationDto> entities) {
    SearchListResponseDto responseDto = new SearchListResponseDto();
    responseDto.setEmails(entities);
    responseDto.configureAsSucess();
    return responseDto;
  }

  @ExceptionHandler(BusinessException.class)
  public ResponseEntity<ErrorResponseDto> handleException(BusinessException e) {
    return new ResponseEntity<>(
        new ErrorResponseDto(HttpStatus.NOT_FOUND.value(), e.getMessage()), new HttpHeaders(), HttpStatus.NOT_FOUND);
  }

}
