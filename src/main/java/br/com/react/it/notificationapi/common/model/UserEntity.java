package br.com.react.it.notificationapi.common.model;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@Entity
@Table(schema = "apicommunication", name = "user")
public class UserEntity implements Serializable {
  private static final long serialVersionUID = 9146450973035948082L;

  @Id
  @SequenceGenerator(name = "seq_id_user", sequenceName = "apicommunication.seq_id_user", allocationSize = 1)
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_id_user")
  @Column(name = "id_user")
  private Long idUser;

  @Column(name = "login")
  private String login;

  @Column(name = "email")
  private String email;

  @Column(name = "password")
  private String password;

  @Column(name = "status")
  private Integer status;

  @Column(name = "api_key")
  private String apiKey;

  @Column(name = "date_created")
  private LocalDateTime dateCreated = LocalDateTime.now();

  public Long getIdUser() {
    return idUser;
  }

  public void setIdUser(Long idUser) {
    this.idUser = idUser;
  }

  public String getLogin() {
    return login;
  }

  public void setLogin(String login) {
    this.login = login;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public Integer getStatus() {
    return status;
  }

  public void setStatus(Integer status) {
    this.status = status;
  }

  public String getApiKey() {
    return apiKey;
  }

  public void setApiKey(String apiKey) {
    this.apiKey = apiKey;
  }

  public LocalDateTime getDateCreated() {
    return dateCreated;
  }

  public void setDateCreated(LocalDateTime dateCreated) {
    this.dateCreated = dateCreated;
  }

  @Override
  public String toString() {
    return "UserEntity{" +
        "idUser=" + idUser +
        ", email='" + email + '\'' +
        ", status=" + status +
        ", apiKey='" + apiKey + '\'' +
        ", dateCreated=" + dateCreated +
        '}';
  }
}
