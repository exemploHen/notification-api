package br.com.react.it.notificationapi.common.service;

import br.com.react.it.notificationapi.common.model.UserEntity;
import br.com.react.it.notificationapi.common.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class UserService {

  private UserRepository userRepository;

  private PasswordEncoder passwordEncoder;

  private static final Integer STATUS_ACTIVE = 1;

  @Autowired
  public UserService(UserRepository userRepository, PasswordEncoder passwordEncoder) {
    this.userRepository = userRepository;
    this.passwordEncoder = passwordEncoder;
  }

  public UserEntity registerUser(UserEntity entity) {
    entity.setPassword(passwordEncoder.encode(entity.getPassword()));
    return userRepository.save(entity);
  }

  public UserEntity retrieveUserAndCheckPassword(String email, String passwordSent) throws Exception {
    UserEntity entity = findValidUserByEmail(email);
    if (!checkPassword(passwordSent, entity)) {
      throw new Exception("Passwords don't match");
    }
    return entity;
  }

  private boolean checkPassword(String passwordSent, UserEntity entity) {
    return passwordEncoder.matches(passwordSent, entity.getPassword());
  }

  public boolean isApiKeyActive(String apiKey) {
    return getByApiKeyAndStatus(apiKey) != null;
  }

  public UserEntity findValidUserByEmail(String email) throws Exception {
    UserEntity entity = getByEmail(email);
    verifyValidEntity(entity);
    return entity;
  }

  private void verifyValidEntity(UserEntity entity) throws Exception {
    if (entity == null) {
      throw new Exception("User not found");
    }
  }

  public UserEntity searchUserByApiKey(String apiKey) throws Exception {
    UserEntity entity = getByApiKey(apiKey);
    verifyValidEntity(entity);
    return entity;
  }

  private UserEntity getByApiKey(String apiKey) {
    return userRepository.findByApiKey(apiKey);
  }

  private UserEntity getByEmail(String email) {
    return userRepository.findByEmail(email);
  }

  private UserEntity getByApiKeyAndStatus(String apiKey) {
    return userRepository.findByApiKeyAndStatus(apiKey, STATUS_ACTIVE);
  }
}
