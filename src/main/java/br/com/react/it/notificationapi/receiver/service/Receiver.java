package br.com.react.it.notificationapi.receiver.service;

import br.com.react.it.notificationapi.common.dto.EmailQueueDto;
import br.com.react.it.notificationapi.common.exception.BusinessException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Service;

@Service
public class Receiver {

  private MailService mailService;

  @Autowired
  public Receiver(MailService mailService) {
    this.mailService = mailService;
  }

  @JmsListener(destination = "mailbox", containerFactory = "mailboxListener")
  public void receiveMessage(EmailQueueDto queueDto) throws BusinessException {
    mailService.sendEmail(queueDto);
  }

  @JmsListener(destination = "priority", containerFactory = "priorityListener")
  public void receivePriorityMessage(EmailQueueDto queueDto) {
    mailService.sendPrioritaryEmail(queueDto);
  }
}
