## Configurando a aplicação
1) Anexado ao projeto, existe um arquivo .sql que deve ser executado antes de subir a aplicação
2) Para utilizar o Mailgun, é preciso acessar o site: https://signup.mailgun.com/new/signup e fazer o cadastro.
3) Após ativar a conta, é preciso de duas informações relativas ao mailgun, a chave (*API KEY*) e o domínio (*DOMAIN*), que podem ser acessados na *dashboard*
4) Alterar os valores presentes no arquivo *application.properties* com os presentes no site.
5) Ao startar a aplicação, deve-se incluir no script uma variável de ambiente: o caminho para o arquivo *application.properties*, que fica externo à aplicação
6) Para iniciar a aplicação, rode o comando:
 - java -jar notification-api-*versao*.jar -Dproperties.location=/caminho/absoluto/para/arquivo 